/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employeesort;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 *
 * @author arad
 */
class Personele implements Comparable<Personele>{
    String name;
    String lastName;
    int age;
    double salary;
    Personele(String name,String lastName,int age,double salary){
       this.name=name;
       this.lastName=lastName;
       this.age=age;
       this.salary=salary;
    }

    

//    public static Comparator<Personele> compareByLastNmae=new Comparator<Personele>(){
//        @Override
//        public int compare(Personele p1, Personele p2) {
//            return p1.lastName.toLowerCase().compareTo(p2.lastName.toLowerCase()); //To change body of generated methods, choose Tools | Templates.
//        }
//    };
    public static Comparator<Personele> compareByAge=new Comparator<Personele>(){
        @Override
        public int compare(Personele p1, Personele p2) {
            return p1.age -p2.age; //To change body of generated methods, choose Tools | Templates.
        }
    };
    public static Comparator<Personele> compareByLastName=new Comparator<Personele>(){
        @Override
        public int compare(Personele p1, Personele p2) {
            return p1.lastName.toLowerCase().compareTo(p2.lastName.toLowerCase()); //To change body of generated methods, choose Tools | Templates.
        }
    };
    public static Comparator<Personele> comparatorBySalary = new Comparator<Personele>() {
        @Override
        public int compare(Personele p1, Personele p2) {

            if (p1.salary == p2.salary) {

                return 0;
            } else if (p2.salary < p1.salary) {
                return 1;
            } else {
                return -1;
            }
        }

    };

    @Override
    public int compareTo(Personele p) {
        return name.toLowerCase().compareTo(p.name.toLowerCase());  //To change body of generated methods, choose Tools | Templates.
    }
    public String toString(){
//    return name+" "+lastName+" is "+age+"y/o" +"Salary is :  "+salary;
    return String.format ("%s:%s:%d:%.2f ",this.name,this.lastName,this.age,this.salary);
    }
    
}
   
    

public class EmployeeSort {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        ArrayList<Personele> emp=new ArrayList<Personele>();
        emp.add(new Personele("Arad","bcd",18,2000));
        emp.add(new Personele("Dara","zbcd",22,2000));
        emp.add(new Personele("Lona","abcd",45,4000));
        emp.add(new Personele("Arad","bcd",34,3000));
        emp.add(new Personele("Arad","wbcd",65,2000));
        System.out.println("orginal");
        for(Personele p:emp){
            System.out.println(p);
            
        }
//        Collections.sort(emp,Personele.compareBySalary);
//        System.out.println();
//        for(Personele p:emp){
//            System.out.println(p);
//            
//        }
//        Collections.sort(emp,Personele.compareByLastName);
//        System.out.println();
//        for(Personele p:emp){
//            System.out.println(p);
//            
//        }
        
        Collections.sort(emp,Personele.comparatorBySalary);
        System.out.println();
        for(Personele p:emp){
            System.out.println(p);
        }
    }
    
}
