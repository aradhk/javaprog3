/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enumexcerssice;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author arad
 */
public class EnumExcerssice {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException {
        // TODO code application logic here
         ArrayList<Person> list = new ArrayList<>();
         Scanner s=new Scanner(new File("input.txt"));
         while(s.hasNextLine()){
         String line=s.nextLine();
         String data[]=line.split(";");
         Person p=new Person();
         p.name=data[0];
         p.title=Person.Title.valueOf(data[1]);
         p.diplom=Person.Diplom.valueOf(data[2]);
         list.add(p);
         
         }
         for (Person p : list) {
            System.out.println(p);
        }
    }
    
}
