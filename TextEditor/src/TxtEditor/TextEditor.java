/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TxtEditor;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Scanner;
import javafx.stage.FileChooser;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Ahmed
 */
public class TextEditor extends javax.swing.JFrame {
    
   
     

    /**
     * Creates new form TextEditor
     */
    public TextEditor() {
        initComponents();
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
        
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Text Files .txt and .text", "txt","text");
        filechooserLoad.setFileFilter(filter);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuItem2 = new javax.swing.JMenuItem();
        filechooserLoad = new javax.swing.JFileChooser();
        filechooserSave = new javax.swing.JFileChooser();
        jScrollPane1 = new javax.swing.JScrollPane();
        textarea = new javax.swing.JTextArea();
        lbPath = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        mbFile = new javax.swing.JMenu();
        miFileOpen = new javax.swing.JMenuItem();
        miFileSaveAs = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        miFileExit = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();

        jMenuItem2.setText("jMenuItem2");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(400, 400));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        textarea.setColumns(20);
        textarea.setRows(5);
        jScrollPane1.setViewportView(textarea);

        getContentPane().add(jScrollPane1, java.awt.BorderLayout.CENTER);

        lbPath.setText("No File");
        getContentPane().add(lbPath, java.awt.BorderLayout.PAGE_END);

        mbFile.setText("File");
        mbFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mbFileActionPerformed(evt);
            }
        });

        miFileOpen.setText("Open");
        miFileOpen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miFileOpenActionPerformed(evt);
            }
        });
        mbFile.add(miFileOpen);

        miFileSaveAs.setText("Save as");
        miFileSaveAs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miFileSaveAsActionPerformed(evt);
            }
        });
        mbFile.add(miFileSaveAs);
        mbFile.add(jSeparator1);

        miFileExit.setText("Exit");
        miFileExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miFileExitActionPerformed(evt);
            }
        });
        mbFile.add(miFileExit);

        jMenuBar1.add(mbFile);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void mbFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mbFileActionPerformed
     
        
    }//GEN-LAST:event_mbFileActionPerformed

    private void miFileExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miFileExitActionPerformed
        this.setVisible(false);
        dispose();
    }//GEN-LAST:event_miFileExitActionPerformed

    private void miFileOpenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miFileOpenActionPerformed
       // TODO add your handling code here:
        if (filechooserLoad.showOpenDialog(this)== JFileChooser.APPROVE_OPTION){
            
            File file = filechooserLoad.getSelectedFile();
          try( Scanner  fileInput= new Scanner(file)) {
              fileInput.useDelimiter("\\Z");
              String fileContent = fileInput.next();
              textarea.setText(fileContent);
              lbPath.setText(file.getAbsolutePath());
          }catch(IOException ex){
              JOptionPane.showMessageDialog(this,
                        ex.getMessage(),
                        "File reading error",
                        JOptionPane.ERROR_MESSAGE);
          }
            
            
        }
    }//GEN-LAST:event_miFileOpenActionPerformed

    private void miFileSaveAsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miFileSaveAsActionPerformed
        // TODO add your handling code here:
        
        int n = JOptionPane.showConfirmDialog(this,
    "Would you like to save as the file","Sava As Question",JOptionPane.YES_NO_OPTION);

            if (n == JOptionPane.YES_OPTION){
                if (filechooserLoad.showSaveDialog(this)== JFileChooser.APPROVE_OPTION){
                        
                try(FileWriter fw = new FileWriter(filechooserLoad.getSelectedFile())){
                fw.write(textarea.getText());
                
                
            } catch(IOException ex) {
               JOptionPane.showMessageDialog(this,
                        ex.getMessage(),
                        "File writing error",
                        JOptionPane.ERROR_MESSAGE);
            }
            }
        }
    }//GEN-LAST:event_miFileSaveAsActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // TODO add your handling code here:
        int confirmed = JOptionPane.showConfirmDialog(null, 
        "Are you sure you want to exit the program?", "Exit Program Message Box",
        JOptionPane.YES_NO_OPTION);

    if (confirmed == JOptionPane.YES_OPTION) {
        this.setVisible(false);
      dispose();
        }    else{
        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        }
    }//GEN-LAST:event_formWindowClosing

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TextEditor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TextEditor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TextEditor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TextEditor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TextEditor().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JFileChooser filechooserLoad;
    private javax.swing.JFileChooser filechooserSave;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JLabel lbPath;
    private javax.swing.JMenu mbFile;
    private javax.swing.JMenuItem miFileExit;
    private javax.swing.JMenuItem miFileOpen;
    private javax.swing.JMenuItem miFileSaveAs;
    private javax.swing.JTextArea textarea;
    // End of variables declaration//GEN-END:variables
}
