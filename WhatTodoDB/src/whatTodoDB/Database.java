/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package whatTodoDB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;




/**
 *
 * @author arad
 */
public class Database {
    public static final String DBUSER = "arad";
    public static final String DBPASS = "QNXVasAxKWa90n2H";
    
    private Connection conn;
     public Database() throws SQLException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost/arad", DBUSER, DBPASS);
        } catch (ClassNotFoundException ex) {
            throw new SQLException("Driver class not found", ex);
        }
    }
     
     public void addTodos(Todos t) throws SQLException {
        String sql = "INSERT INTO todos (task, dueDate, isDone) VALUES (?, ?, ?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, t.task);
            stmt.setDate(2, new java.sql.Date(t.dueDate.getTime()));
            // convert java.util.Date to java.sql.Date by asking for time as long value
            stmt.setLong(3,t.isDone);
            stmt.executeUpdate();
        }
    }
      public void updateTodos(Todos t) {
        throw new RuntimeException("Update record not implemented yet");
    }
      public void deleteTodos(long id) throws SQLException {
        String sql = "DELETE FROM Todos WHERE ID=?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setLong(1, id);
            stmt.executeUpdate();
        }
    }
      public ArrayList<Todos> getAllTodos() throws SQLException {
        ArrayList<Todos> list = new ArrayList<>();
        String sql = "SELECT * FROM todos";
        try (Statement stmt = conn.createStatement(); ResultSet result = stmt.executeQuery(sql)) {
            while (result.next()) {
                Todos t = new Todos();
                t.id= result.getLong("ID");
                t.task= result.getString("task");
                t.dueDate = result.getDate("dueDate");
                t.isDone= result.getInt("isDone");
                list.add(t);
            }
        }
        return list;
}
}
