/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz2Travel;

import java.util.Date;

/**
 *
 * @author ipd11
 */
public class Trip {
    public Trip(String dest, String name, String pass, Date depa, Date ret) {

        this.dest = dest;
        this.name = name;
        this.pass = pass;
        this.depa = depa;
        this.ret = ret;
    }
    
    private String dest;
    private String name;
    private String pass;
    private Date depa;
    private Date ret;
    
   @Override
    public String toString() {
        return String.format("%s(%s) to %s from %s to %s", dest, name, pass, depa, ret);
    }

    public String getDest() {
        return dest;
    }

    public void setDest(String dest) {
        if (dest.length() < 2 || dest.length() > 30 ) {
            throw new IllegalArgumentException("Name must be between 1 and 50 characters long");
                    
        }
        this.dest = dest;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name.length() < 2 || name.length() > 30 ) {
            throw new IllegalArgumentException("Name must be between 1 and 50 characters long");
                    
        }
        this.name = name;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        if (!pass.matches("[A-Z][A-Z][0-9]{6}")) {
            throw new IllegalArgumentException("Passport must be in AA123456 format");
        }
        this.pass = pass;
    }

    public Date getDepa() {
        return depa;
    }

    public void setDepa(Date depa) {
        this.depa = depa;
    }

    public Date getRet() {
        return ret;
    }

    public void setRet(Date ret) {
        this.ret = ret;
    }
    
    
}
