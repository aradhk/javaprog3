/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peapleList;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.Scanner;

/**
 *
 * @author ipd11
 */
public class PesonFileAccess {
   final  static String FILE_NAME="people.txt";
   public static void savePersonListToFile( ArrayList<Person> list) throws IOException {
   
       
     try(PrintWriter out=new PrintWriter(new BufferedWriter(new FileWriter(FILE_NAME,false)))){
         for(Person p:list){
             String line=String.format("$s;%d;%s",p.getName(),p.getAge(),p.getPostalCode() );
             out.println(line);
         }
     }  
   
   }
   public static ArrayList<Person> loadPersonListFromFile(StringBuilder Warnings) throws IOException {
       ArrayList result=new ArrayList();
       File file=new File(FILE_NAME);
       if(!file.exists()){
           return result;
       }
        try(Scanner fileInput=new Scanner(file)){
            while(fileInput.hasNextLine()){
                String line=fileInput.nextLine();
                String[]data=line.split(";");
                if(data.length!=3){
                   System.out.println("\n* Invalid number of fields in line,skipping: "+line);
                   continue;
                }
                String name=data[0];
                String strAge=data[1];
                String postalCode=data[2];
                int age=0;
                try{
                 age=Integer.parseInt(strAge);
            }catch(NumberFormatException ex){
                    System.out.println("\n* Invalid age,not a integer,skipping: "+line);
                    continue;
                    }
                //
                Person p=null;
                try{
                    p=new Person(name,age,postalCode);
                    
                }catch(IllegalArgumentException ex){
                    System.out.println("\n* Exception creating person from line: "+line);
                    System.out.println(ex.getMessage());
                    continue;
                }
                result.add(p);
            }
                }
        return result;
            }
   }
        
    



    