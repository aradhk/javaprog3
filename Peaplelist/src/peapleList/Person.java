/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peapleList;

/**
 *
 * @author ipd11
 */
public class Person {
    
    public Person(String name ,int age,String postalCode) {
        setName(name);     
        setAge(age);
        setPostalCode(postalCode);
            
    }
    
    private String name;
    private int age;
    private String postalCode;
    @Override
    public String toString(){
        return String.format("%s is %d %sy/o", name, age,postalCode);
    }
    public String getName(){
    return name;
    }
    public String getPostalCode(){
    return postalCode;
    }
    public int getAge(){
    return age;
    }
  public void setName(String name){
        if (name.length()<1 || name.length()>50|| name.contains(";")){
        throw new IllegalArgumentException("Nmae must be between 1 and 50 character long");
        }
        this.name=name;
  }
        public void setPostalCode(String postalCode){
        if (!postalCode.matches("^[A-Z][0-9][A-Z] [0-9][A-Z][0-9]$")){
        throw new IllegalArgumentException("Postal code must have this format: A1B1C3");
        }
        this.postalCode=postalCode;
    
    }
    public void setAge(int age){
        if(age<1||age>150){
        throw new IllegalArgumentException("Age must be between 1 and 150 ");
        }
     this.age=age;
    }
}
