/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry;

/**
 *
 * @author arad
 */
public class Hexagon extends GeObj {
    private double side;
    private double h;
    private static String dimention="tow dimention";

    public Hexagon(String color, String name,double side,double h) {
        super(color, name);
        this.side = side;
        this.h = h;
        
    }
    public static String getDimention() { 
       
       return Hexagon.dimention; 
    }

    @Override
    public double getVolume() {
     // return (3*Math.sqrt(3)*this.side*this.side*this.h)/2.0; //To change body of generated methods, choose Tools | Templates.
    return 0;
    }

    @Override
    public double getSurface() {
        return (3*Math.sqrt(3)*this.side*this.side)/2.0; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getVerticesCount() {
      return 0; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getEdgeCount() {
       return 8; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getPerimeter() {
        return this.side*8; //To change body of generated methods, choose Tools | Templates.
    }
    
}
