/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author arad
 */
public class Geometry {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        ArrayList<GeObj> list = new ArrayList<>();
        try (Scanner fileInput = new Scanner(new File("input.txt"))) {
            while (fileInput.hasNextLine()) {
                String line = fileInput.nextLine();
                String data[] = line.split(";");
                try{
                switch (data[0]) {
                    
                    case "Rectangle":
                        if (data.length != 4||data[1].isEmpty() || data[2].isEmpty()||data[3].isEmpty()) {
                            System.out.println("\n*Invalid number of fields in line, Skipping:" + line);
                            continue;
                        }
                        Rectangle r = new Rectangle(data[1], Double.parseDouble(data[2]), Double.parseDouble(data[3]));
                        list.add(r);
                        break;
                    case "Circle":
                        if (data.length != 3 || data[1].isEmpty() || data[2].isEmpty()) {
                            System.out.println("\n*Invalid number of fields in line, Skipping:" + line);
                            continue;
                        }
                        Circle cir = new Circle(data[1], Double.parseDouble(data[2]));
                        list.add(cir);
                        break;
                    case "Square":
                        if (data.length != 3 ||data[1].isEmpty() || data[2].isEmpty()) {
                            System.out.println("\n*Invalid number of fields in line, Skipping:" + line);
                            continue;
                        }
                        Square s = new Square(data[1], Double.parseDouble(data[2]));
                        list.add(s);
                        break;
                    case "Sphere":
                        if (data.length != 3 ||data[1].isEmpty() || data[2].isEmpty()) {
                            System.out.println("\n*Invalid number of fields in line, Skipping:" + line);
                            continue;
                        }
                        Sphere sp = new Sphere(data[1], Double.parseDouble(data[2]));
                        list.add(sp);
                        break;
                    case "Point":
                        if (data.length != 2 || data[1].isEmpty() ) {
                            System.out.println("\n*Invalid number of fields in line, Skipping:" + line);
                            continue;
                        }
                        Point p = new Point(data[1]);
                        list.add(p);
                        break;
                    default:
                        System.out.println("Invalid line: " + line);
                        break;
                }
            }catch(NumberFormatException ex){
                    System.out.println("Invalid input format in line:("+line+") "+ex.getMessage());
                    }
            }
        } catch (FileNotFoundException ex) {
            System.out.println("file not found," + ex.getMessage());
        }
        System.out.println("---------------------------------------------");
        for (GeoObj g : list){
            g.print();
    }
    
}
}
