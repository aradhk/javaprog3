/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry;

/**
 *
 * @author arad
 */
public class Sphere extends GeObj {
     private double r;
     private static String dimention="Tree Dimention";

    public Sphere(String color, String name,double r) {
        super(color, name);
        this.r = r;
        
    }
    public static String getDimention() { 
       
       return Sphere.dimention; 
    }

    @Override
    public double getVolume() {
        return (Math.PI*this.r*this.r*this.r*4)/3.0; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getSurface() {
        return (4*Math.PI*this.r*this.r); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getVerticesCount() {
       return 0; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getEdgeCount() {
        return 0; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getPerimeter() {
        return 0; //To change body of generated methods, choose Tools | Templates.
    }
    
}
