/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry;

/**
 *
 * @author arad
 */
public abstract class GeObj {
   private String color;
   private String name;
   

    public GeObj(String color,String name) {
        this.color = color;
        this.name = name;
    }
    public String getColor() {
        if (color.length()<2) {
            throw new IllegalArgumentException("Color length less than 2");
        }
         return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    public abstract double getVolume();
    public abstract double getSurface();
    public abstract int getVerticesCount();
    public abstract int getEdgeCount();
    public abstract double getPerimeter();
}
