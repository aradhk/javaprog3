/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry;

/**
 *
 * @author arad
 */
public class Rectangle extends GeObj{
    private double side1;
    private double side2;
    private static String dimention="tow dimention";

    public Rectangle(String color, String name,double side1,double side2) {
        super(color, name);
        this.side1 = side1;
        this.side2 = side2;
    }
    public static String getDimention() { 
       
       return Rectangle.dimention; 
    }

    @Override
    public double getVolume() {
        return 0;//To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getSurface() {
       return this.side1*this.side2; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getVerticesCount() {
        return 0; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getEdgeCount() {
     return 4; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getPerimeter() {
         return 2*(this.side1+this.side2); //To change body of generated methods, choose Tools | Templates.
    }
    
}
