/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry;

/**
 *
 * @author arad
 */
public class Cube extends GeObj {
    private double side;
    private static String dimention="Tree Dimention";

    public Cube(String color, String name,double side) {
        super(color, name);
        this.side = side;
        dimention=dimention;
    }
     public static String getDimention() { 
       
       return Cube.dimention; 
    }

    @Override
    public double getVolume() {
        return this.side*this.side*this.side; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getSurface() {
        return 6*this.side*this.side; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getVerticesCount() {
       return 0; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getEdgeCount() {
        return 0; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getPerimeter() {
        return 0; //To change body of generated methods, choose Tools | Templates.
    }
    
}
