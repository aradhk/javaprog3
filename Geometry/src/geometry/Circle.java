/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry;

/**
 *
 * @author arad
 */
public class Circle extends GeObj {
    private double r;
    private static String dimention="tow dimention";

 public Circle(String color, String name,double r) {
        super(color, name);
        this.r = r;
    }
 public static String getDimention() { 
       
       return Circle.dimention; 
    }


    @Override
    public double getVolume() {
      return 0; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getSurface() {
        return Math.PI*this.r*this.r;//To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getVerticesCount() {
        return 0; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getEdgeCount() {
        return 0; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getPerimeter() {
        return Math.PI*this.r*2; //To change body of generated methods, choose Tools | Templates.
    }
    
}
