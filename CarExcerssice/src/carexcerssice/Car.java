/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carexcerssice;

/**
 *
 * @author arad
 */
public class Car implements Comparable<Car>{
    
    String make; // e.g. BMW, Toyota, etc.
    String model; // e.g. X5, Corolla, etc.
    int maxSpeedKmph; // maximum speed in km/h
    double secTo100Kmph; // accelleration time 1-100 km/h in seconds
    double litersPer100km; // economy: liters of gas per 100 km
    private static int counter=0;
    private int uniqueId =0;
    public Car(String make, String model, int maxSpeedKmph, double secTo100Kmph, double litersPer100km) {
        this.make = make;
        this.model = model;
        this.maxSpeedKmph = maxSpeedKmph;
        this.secTo100Kmph = secTo100Kmph;
        this.litersPer100km = litersPer100km;
        counter++;
        this.uniqueId=counter;
    }
    public static int getCount() { 
       
       return Car.counter; 
    }

    
   public int getUnique() { 
   return uniqueId;
   }

    @Override
    public int compareTo(Car t) {
        int comp = this.make.toUpperCase().compareTo(t.make.toUpperCase());

            if (comp != 0) 
                return comp;
            comp = model.compareTo(t.model);
    if (comp != 0) return comp;

    return Integer.compare(maxSpeedKmph, t.maxSpeedKmph);

     

            }
    public String toString() {
        return String.format ("%d:%s:%s:%d:%.2f:%.2f ",this.getUnique(),this.make,this.model,this.maxSpeedKmph,this.secTo100Kmph,this.litersPer100km) ;
    }
//To change body of generated methods, choose Tools | Templates.
    }
    

