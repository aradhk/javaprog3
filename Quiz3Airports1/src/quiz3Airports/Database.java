/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz3Airports;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author ipd11
 */
public class Database {
    public static final String DBUSER = "quiz3airports";
    public static final String DBPASS = "MzGVekdRYSO9FPhT";

    private Connection conn;

    public Database() throws SQLException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost/quiz3airports", DBUSER, DBPASS);
        } catch (ClassNotFoundException ex) {
            throw new SQLException("Driver class not found", ex);
        }
    
    }
    public void addAirports(Airports  a) throws SQLException {
        String sql = "INSERT INTO airports (city, code, elevation,isInternational,latitude,longitude) VALUES (?, ?, ?,?,?,?)";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setString(1, a.city);
            stmt.setString(2, a.code);
            stmt.setLong(3,a.elevation );
            stmt.setInt(4,(( a.isInternational)==true)? 1 : 0);
            stmt.setDouble(5,a.latitude);
            stmt.setDouble(6,a.longitude);
            
            stmt.executeUpdate();
        }
    }
        public void deleteAirports(long id) throws SQLException {
        String sql = "DELETE FROM airports WHERE ID=?";
        try (PreparedStatement stmt = conn.prepareStatement(sql)) {
            stmt.setLong(1, id);
            stmt.executeUpdate();
        }
    }
        public ArrayList<Airports> getAllAirports() throws SQLException {
        ArrayList<Airports> list = new ArrayList<>();
        String sql = "SELECT * FROM airports";
        try (Statement stmt = conn.createStatement(); ResultSet result = stmt.executeQuery(sql)) {
            while (result.next()) {
                Airports a = new Airports();
                a.id = result.getLong("ID");
                a.city = result.getString("city");
                a.code = result.getString("code");
                a.elevation = result.getInt("elevation");
                a.isInternational=(result.getInt("isInternational")==1 ? true : false);
                a.latitude=result.getDouble("latitude");
                a.longitude=result.getDouble("longitude");
                list.add(a);
                
                
            
        }
        return list;
    }
        }
}

