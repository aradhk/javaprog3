
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.Buffer;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

public class MyFriends extends javax.swing.JFrame {

    DefaultListModel<String> friendListModel = new DefaultListModel<>();

    public MyFriends() {
        initComponents();
        loadListFromFile();
    }

    public void saveListToFile() {
        ArrayList<String> friendList = new ArrayList<>();
        for (int i = 0; i < lstNameFriends.getModel().getSize(); i++) {
            friendList.add(lstNameFriends.getModel().getElementAt(i));
            String line = String.format("%s\n", friendList.get(i).toString());
            try {
                PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("Friend.txt", true)));
                out.print(line);
                out.close();

            } catch (IOException e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(this, e.getMessage(), "File Writing Error",
                        JOptionPane.ERROR_MESSAGE);
            }
        }

    }

    public void loadListFromFile() {
        String line;
        try {
            BufferedReader in = new BufferedReader(new FileReader("Friend.txt"));
            while ((line = in.readLine()) != null) {
                friendListModel.addElement(line);

            }
        } catch (IOException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, e.getMessage(), "File Loading Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        tfAddName = new javax.swing.JTextField();
        btAddFriend = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstNameFriends = new javax.swing.JList<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Name");

        btAddFriend.setText("Add Friend");
        btAddFriend.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btAddFriendActionPerformed(evt);
            }
        });

        lstNameFriends.setModel(friendListModel);
        lstNameFriends.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstNameFriendsMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(lstNameFriends);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(tfAddName, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btAddFriend))
                    .addComponent(jScrollPane1))
                .addContainerGap(21, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(tfAddName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btAddFriend))
                .addGap(27, 27, 27)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(92, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btAddFriendActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btAddFriendActionPerformed
        String name = tfAddName.getText();
        if (name.isEmpty()) {
            return;
        }
        friendListModel.addElement(name);
        tfAddName.setText("");
        saveListToFile();


    }//GEN-LAST:event_btAddFriendActionPerformed

    private void lstNameFriendsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstNameFriendsMouseClicked
        if (evt.getClickCount() == 2 && evt.getButton() == 1) {
            System.out.println("Left button double clicked");
            String name = lstNameFriends.getSelectedValue();
            JOptionPane.showMessageDialog(this, name);
        }

    }//GEN-LAST:event_lstNameFriendsMouseClicked

    public static void main(String args[]) {
     
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MyFriends().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btAddFriend;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JList<String> lstNameFriends;
    private javax.swing.JTextField tfAddName;
    // End of variables declaration//GEN-END:variables
}
