/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometry2;



/**
 *
 * @author arad
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
abstract class GeoObj {
    private String color;
    String shape;
    // TODO: Throw IllegalArgumentException if color length is less than 2 chars
    public GeoObj(String color) {
        if (color.length() < 2) {
            throw new IllegalArgumentException("Color must be more than 2 characters");
        }
        this.color = color;
    }
    // TODO: declare all other methods as abstract - listed in the photo
    abstract double getSurface();
    abstract double getCircumPerimeter();
    abstract int getVerticesCount();
    abstract int getEdgesCount();
    public void print() {
        System.out.printf("Shape: %s, Color : %s, Surface: %.2f, Cirumference/Perimeter: %.2f, Number of Vertices: %d ,"
                + " Number of Edges: %d \n", shape, color, getSurface(), getCircumPerimeter(), getVerticesCount(), getEdgesCount());
    }
}
class Point extends GeoObj {
    Point(String color) {
        super(color);
        this.shape = "Point";
    }
    @Override
    double getSurface() {
        return 0.0;
    }
    @Override
    double getCircumPerimeter() {
        return 0.0;
    }
    @Override
    int getVerticesCount() {
        return 1;
    }
    @Override
    int getEdgesCount() {
        return 1;
    }
}
class Rectangle extends GeoObj {
    double height;
    double width;
    public Rectangle(String color, double height, double width) {
        super(color);
        this.height = height;
        this.width = width;
        this.shape = "Rectangle";
    }
    @Override
    double getSurface() {
        return height * width;
    }
    @Override
    double getCircumPerimeter() {
        return 2.0 * (height + width);
    }
    @Override
    int getVerticesCount() {
        return 4;
    }
    @Override
    int getEdgesCount() {
        return 4;
    }
}
class Circle extends GeoObj {
    public Circle(String color, double radius) {
        super(color);
        this.radius = radius;
        this.shape = "Circle";
    }
    double radius;
    @Override
    double getSurface() {
        return Math.PI * Math.pow(radius, 2);
    }
    @Override
    double getCircumPerimeter() {
        return 2.0 * (Math.PI) * radius;
    }
    @Override
    int getVerticesCount() {
        return 0;
    }
    @Override
    int getEdgesCount() {
        return 0;
    }
}
class Sphere extends GeoObj {
    double radius;
    public Sphere(String color, double radius) {
        super(color);
        this.radius = radius;
        this.shape = "Sphere";
    }
    @Override
    double getSurface() {
        return 4 * Math.PI * Math.pow(radius, 2);
    }
    @Override
    double getCircumPerimeter() {
        return 2 * Math.PI * radius;
    }
    @Override
    int getVerticesCount() {
        return 0;
    }
    @Override
    int getEdgesCount() {
        return 0;
    }
}
class Square extends Rectangle {
    double edgeSize;
    public Square(String color, double edgeSize) {
        super(color, edgeSize, edgeSize);
        this.edgeSize = edgeSize;
    }
}

    
   

public class Geometry2 {
static ArrayList<GeoObj> list = new ArrayList<>();
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Rectangle rec = new Rectangle("white", 2, 4);
//        rec.print();
//        Circle c = new Circle(5, "Red");
//        c.print();
        try (Scanner fileInput = new Scanner(new File("input.txt"))) {
            while (fileInput.hasNextLine()) {
                String line = fileInput.nextLine();
                String data[] = line.split(";");
                try{
                switch (data[0]) {
                    
                    case "Rectangle":
                        if (data.length != 4||data[1].isEmpty() || data[2].isEmpty()||data[3].isEmpty()) {
                            System.out.println("\n*Invalid number of fields in line, Skipping:" + line);
                            continue;
                        }
                        Rectangle r = new Rectangle(data[1], Double.parseDouble(data[2]), Double.parseDouble(data[3]));
                        list.add(r);
                        break;
                    case "Circle":
                        if (data.length != 3 || data[1].isEmpty() || data[2].isEmpty()) {
                            System.out.println("\n*Invalid number of fields in line, Skipping:" + line);
                            continue;
                        }
                        Circle cir = new Circle(data[1], Double.parseDouble(data[2]));
                        list.add(cir);
                        break;
                    case "Square":
                        if (data.length != 3 ||data[1].isEmpty() || data[2].isEmpty()) {
                            System.out.println("\n*Invalid number of fields in line, Skipping:" + line);
                            continue;
                        }
                        Square s = new Square(data[1], Double.parseDouble(data[2]));
                        list.add(s);
                        break;
                    case "Sphere":
                        if (data.length != 3 ||data[1].isEmpty() || data[2].isEmpty()) {
                            System.out.println("\n*Invalid number of fields in line, Skipping:" + line);
                            continue;
                        }
                        Sphere sp = new Sphere(data[1], Double.parseDouble(data[2]));
                        list.add(sp);
                        break;
                    case "Point":
                        if (data.length != 2 || data[1].isEmpty() ) {
                            System.out.println("\n*Invalid number of fields in line, Skipping:" + line);
                            continue;
                        }
                        Point p = new Point(data[1]);
                        list.add(p);
                        break;
                    default:
                        System.out.println("Invalid line: " + line);
                        break;
                }
            }catch(NumberFormatException ex){
                    System.out.println("Invalid input format in line:("+line+") "+ex.getMessage());
                    }
            }
        } catch (FileNotFoundException ex) {
            System.out.println("file not found," + ex.getMessage());
        }
        System.out.println("---------------------------------------------");
        for (GeoObj g : list){
            g.print();
        }
    }
    }
    

