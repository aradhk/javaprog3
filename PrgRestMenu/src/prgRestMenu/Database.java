/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prgRestMenu;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author arad
 */
public class Database {
    public static final String DBUSER = "restaurant";
    public static final String DBPASS = "73lX0CHTHqNcxMN5";
     private Connection conn;

    public Database() throws SQLException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost/restaurant", DBUSER, DBPASS);
        } catch (ClassNotFoundException ex) {
            throw new SQLException("Driver class not found", ex);
        }
}
    public ArrayList<DishMenu> getAllMenu() throws SQLException {
        ArrayList<DishMenu> list = new ArrayList<>();
        String sql = "SELECT * FROM menu";
        try (Statement stmt = conn.createStatement(); ResultSet result = stmt.executeQuery(sql)) {
            while (result.next()) {
                DishMenu d = new DishMenu();
                d.id = result.getLong("ID");
                d.ingredience = result.getString("ingredience");
                d.name= result.getString("name");
                d.price = result.getBigDecimal("price");
                d.vegetarian = result.getInt("vegetarian");
                d.foodType=result.getString("foodType");
                d.calories=result.getInt("calories");
                list.add(d);
            }
        }
        return list;
    }
}
