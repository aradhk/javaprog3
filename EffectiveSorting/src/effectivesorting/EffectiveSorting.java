/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package effectivesorting;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author ipd11
 */
class Person implements Comparable<Person> {
String name;
int age;
Person (String name, int age) {
this.name = name;
this.age = age;
}




        @Override
    public int compareTo(Person o) {
       return age-o.age;
    }
        @Override
    public String toString()
    {
        return name+ "-"+age;
    }
}
public class EffectiveSorting {
    


        

  
    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {
        // TODO code application logic here
        
//       ArrayList<Person> people=new ArrayList<Person>();
//       people.add(new Person("Sarah",30));
//       people.add(new Person("Lyne",35));
//       people.add(new Person("Roza",22));
//       people.add(new Person("Androu",40));
//       people.add(new Person("Dinn",29));
//       people.add(new Person("Reymond",62));
//       people.add(new Person("Sebastian",43));
//      System.out.println("original list: ");
//        for(Person p:people){
//            System.out.print(p+",");
//        }
//        System.out.println("sorted list: ");
//         Collections.sort(people);
//        for(Person p:people){
//           
//            System.out.print(p+",");
//        }
      ArrayList<Person> people=new ArrayList<>();
     people.add(new Person("Jerry",33));
     people.add(new Person("Terry",12));
     people.add(new Person("Barry",25));
     people.add(new Person("olary",27));
        System.out.println("Original order: ");
        for(Person p:people){
            System.out.println(p+",");
        }
        System.out.println("");
        Collections.sort(people);
        System.out.println("By age order: ");
        for(Person p:people){
            System.out.print(p+",");
        }
    
        System.out.println("");
    }


}
