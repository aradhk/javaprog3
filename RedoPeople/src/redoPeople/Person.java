package redoPeople;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author arad
 */
public class Person {
    public Person(String name,int age,String zipCode) {
    setName(name);
    setAge(age);
    setZipCode(zipCode);
}
    private String name;
    private int age;
    private String zipCode;
    
     @Override
     public String toString(){
     return String.format("%s is %s y/0 at %s",name,age,zipCode);
     }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if(name.length()<1 || name.length()>50){
        throw new IllegalArgumentException("Name must be between 1 and 50 character \n +"
                + "and name can not contaln cemicalon \";\"");
        }
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age<1 || age>150){
            throw new IllegalArgumentException("Age must be between 1 and 150");
        }
        
        this.age = age;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        if(!zipCode.matches("^[A-Z][0-9][A-Z] [0-9][A-Z][0-9]$"))
            throw new IllegalArgumentException("zip code must be in A4A 4B6 Format");
        this.zipCode = zipCode;
    }

    
   
    
    
    
    
    
    
    
}
