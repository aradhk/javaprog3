package bestcar;

import java.util.Comparator;

class Car implements Comparable<Car> {

    String make; // e.g. BMW, Toyota, etc.
    String model; // e.g. X5, Corolla, etc.
    int maxSpeedKmph; // maximum speed in km/h
    double secTo100Kmph; // accelleration time 1-100 km/h in seconds
    double litersPer100km; // economy: liters of gas per 100 km
    int uniqueId;//
    private static int counter;
   
    public static int  getCount(){

    return counter;
    }
    public  int  uniqueId(){

    return uniqueId;
    }
    public Car(String make, String model, int maxSpeedKmph, double secTo100Kmph, double litersPer100km) {
        counter++;
        uniqueId=counter;
        this.make = make;
        this.model = model;
        this.maxSpeedKmph = maxSpeedKmph;
        this.secTo100Kmph = secTo100Kmph;
        this.litersPer100km = litersPer100km;
    }
    
    public String toString()
    {
        return make+ "-"+model+"-"+maxSpeedKmph+"-"+secTo100Kmph+"-"+litersPer100km;
    }
    

    @Override
    public int compareTo(Car o) {
        // compare in case-insensitive manner
        int result = make.toLowerCase().compareTo(o.make.toLowerCase());
        if (result != 0) {
            return result;
        }
        // compare in case-insensitive manner
        return model.toLowerCase().compareTo(o.model.toLowerCase());
    }

    // comparator as an anonymous class
    static Comparator<Car> comparatorByMaxSpeed = new Comparator<Car>() {

        @Override
        public int compare(Car o1, Car o2) {
            return o1.maxSpeedKmph - o2.maxSpeedKmph;
        }
    };
    static Comparator<Car> CarComparatorByAccelleration = new Comparator<Car>(){
        @Override
        public int compare(Car t, Car t1) {
            int result = Double.compare(t.secTo100Kmph, t1.secTo100Kmph);
           if (result == 0) {
                return 0;
            } else if (result>0) {
                return 1;
            } else {
                return -1;
            }
        }
      
        };
}
// Usage: Collections.sort(list, Car.comparatorByMaxSpeed);
// Usage (if no static field): Collections.sort(list, new CarComparattorByMaxSpeed());

