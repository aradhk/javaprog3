/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bestcar;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

/**
 *
 * @author ipd11
 */
class Car implements Comparable<Car> {

    String make; // e.g. BMW, Toyota, etc.
    String model; // e.g. X5, Corolla, etc.
    int maxSpeedKmph; // maximum speed in km/h
    double secTo100Kmph; // accelleration time 1-100 km/h in seconds
    double litersPer100km; // economy: liters of gas per 100 km
    private static int counter=0;
    private int uniqueId =0;
    
    
    
    public Car(String make, String model, int maxSpeedKmph, double secTo100Kmph, double litersPer100km) {
        this.make = make;
        this.model = model;
        this.maxSpeedKmph = maxSpeedKmph;
        this.secTo100Kmph = secTo100Kmph;
        this.litersPer100km = litersPer100km;
        counter++;
        this.uniqueId=counter;
    }
 public static int getCount() { 
       
       return Car.counter; 
    }

    
   public int getUnique() { 
   return uniqueId;
   }
 
    
    
    
    
    
    @Override
    public int compareTo(Car compareCar) {

        int comp = this.make.toUpperCase().compareTo(compareCar.make.toUpperCase());

            if (comp != 0) {
                return comp;
            } else {

                return this.model.toUpperCase().compareTo(compareCar.model.toUpperCase());

            }

    }
    /*mohemmmmmmm moghayese 3 taeee
    @Override     
public int compareTo(Car c) {
    int i = make.compareTo(c.make);
    if (i != 0) return i;

    i = model.compareTo(c.model);
    if (i != 0) return i;

    return Integer.compare(maxSpeedKmph, c.maxSpeedKmph);
}
   */

    public static Comparator<Car> compMakeModel = new Comparator<Car>() {
        @Override
        public int compare(Car car1, Car car2) {

            int comp = car1.make.toUpperCase().compareTo(car2.make.toUpperCase());

            if (comp != 0) {
                return comp;
            } else {

                return car1.model.toUpperCase().compareTo(car2.model.toUpperCase());

            }
        }

    };
    public static Comparator<Car> comparatorBySecTo100 = new Comparator<Car>() {
        @Override
        public int compare(Car car1, Car car2) {

            if (car1.secTo100Kmph == car2.secTo100Kmph) {

                return 0;
            } else if (car2.secTo100Kmph < car1.secTo100Kmph) {
                return 1;
            } else {
                return -1;
            }
        }

    };
    public static Comparator<Car> comparatorByMaxSpeed = new Comparator<Car>() {
        @Override
        public int compare(Car car1, Car car2) {

            
                return car2.maxSpeedKmph -car1.maxSpeedKmph;
            }       

    };
    public static Comparator<Car> comparatorBylitterPer100 = new Comparator<Car>() {
        @Override
        public int compare(Car car1, Car car2) {

            if (car1.litersPer100km == car2.litersPer100km) {

                return 0;
            } else if (car2.litersPer100km < car1.litersPer100km) {
                return 1;
            } else {
                return -1;
            }
        }

    };

    public String toString() {
        return String.format ("%d:%s:%s:%d:%.2f:%.2f ",this.getUnique(),this.make,this.model,this.maxSpeedKmph,this.secTo100Kmph,this.litersPer100km) ;
    }
    
    
   

}

public class BestCar {

    public static void main(String[] args) {
        ArrayList<Car> garage = new ArrayList<>();
        System.out.println(Car.getCount());
        garage.add(new Car("Toyota", "Corolla", 260, 9.0, 1.8));
        System.out.println(Car.getCount());
        garage.add(new Car("Toyota", "Camry", 240, 10.0, 1.9));
        garage.add(new Car("Hyundai", "Elantra", 230, 14.0, 2.0));
        garage.add(new Car("Hyundai", "Accent", 220, 12.0, 1.7));
        garage.add(new Car("Hyundai", "Azera", 250, 8.0, 2.2));
        garage.add(new Car("BMW", "S325", 290, 6.6, 2.5));
        garage.add(new Car("BMW", "S525", 300, 5.5, 3.0));
        System.out.println(Car.getCount());

        System.out.println("Original Order");
        for (Car c : garage) {
            System.out.println(c + ", ");
        }
        System.out.println("");
        
        Collections.sort(garage);

        System.out.println("sorted by make, model Order");
        for (Car c : garage) {
            System.out.println(c + ", ");
        }
        System.out.println("");

        Collections.sort(garage, Car.comparatorByMaxSpeed);
        System.out.println("--------------------------\nsorted by Max Speed \n---------------------------");
        for (Car c : garage) {
            System.out.println(c);
        }
        System.out.println("");
    
    Collections.sort(garage, Car.comparatorBySecTo100);
        System.out.println("---------------------------\nsorted by sec To 100\n---------------------------");
        for (Car c : garage) {
            System.out.println(c);
        }
        System.out.println("");
        Collections.sort(garage, Car.comparatorBylitterPer100);
        System.out.println("---------------------------\nsorted by gas consumption\n---------------------------");
        for (Car c : garage) {
            System.out.println(c);
        }
        System.out.println("");
    }

}
