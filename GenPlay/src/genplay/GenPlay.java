/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package genplay;

import java.util.ArrayList;

 class Stack <T> {
    ArrayList<T> internalArray = new ArrayList<>();
    
	public Stack() {
              
                    }
	public int getHeight() { 
            return internalArray.size();
        }
	public void push(T item) { 
            internalArray.add(item);
        }
	public T pop() { 
            if (internalArray.size()== 0){
                return null;
            }
            return internalArray.remove(internalArray.size()-1);
        }
}

//--------------------------------------------------------------
class Car   {

    String make; // e.g. BMW, Toyota, etc.
    String model; // e.g. X5, Corolla, etc.
    int maxSpeedKmph; // maximum speed in km/h
    double secTo100Kmph; // accelleration time 1-100 km/h in seconds
    double litersPer100km; // economy: liters of gas per 100 km
    private static int counter=0;
    private int uniqueId =0;
    
    
    
    public Car(String make, String model, int maxSpeedKmph, double secTo100Kmph, double litersPer100km) {
        this.make = make;
        this.model = model;
        this.maxSpeedKmph = maxSpeedKmph;
        this.secTo100Kmph = secTo100Kmph;
        this.litersPer100km = litersPer100km;
        counter++;
        this.uniqueId=counter;
    }
 public static int getCount() { 
       
       return Car.counter; 
    }

    
   public int getUnique() { 
   return uniqueId;
   }
   
   public String toString() {
        return String.format ("%d:%s:%s:%d:%.2f:%.2f ",this.getUnique(),this.make,this.model,this.maxSpeedKmph,this.secTo100Kmph,this.litersPer100km) ;
    }
}



public class GenPlay {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Stack<Car> stackOfCars = new Stack<>();
	Stack<String> stackOfStrings = new Stack<>();
	
	System.out.printf("Stack of cars is %d tall, strings is %d tall \n", 
		stackOfCars.getHeight(), stackOfStrings.getHeight());
		
	stackOfCars.push(new Car("Toyota", "Camry", 240, 10.0, 1.9));
	stackOfCars.push(new Car("Hyundai", "Elantra", 230, 14.0, 2.0));
        stackOfCars.push(new Car("Hyundai", "Accent", 220, 12.0, 1.7));
        stackOfCars.push(new Car("BMW", "S325", 290, 6.6, 2.5));
    
        stackOfStrings.push("hi1");
        stackOfStrings.push("hi1");
        stackOfStrings.push("hi1");
        stackOfStrings.push("hi1");
       

	System.out.printf("Stack of cars is %d tall, strings is %d tall \n", 
	stackOfCars.getHeight(), stackOfStrings.getHeight());
        Car c4 = stackOfCars.pop();
        Car c3 = stackOfCars.pop();
	Car c2 = stackOfCars.pop();
	Car c1 = stackOfCars.pop();
	Car c0 = stackOfCars.pop(); // should return null, no more cars left
        System.out.println(c4);
        System.out.println(c3);
        System.out.println(c2);
        System.out.println(c1);
        System.out.println(c0);
        
        
    }
    
}
