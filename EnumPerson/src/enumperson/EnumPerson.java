/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enumperson;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import static java.nio.file.Files.list;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.Scanner;
import javax.swing.JOptionPane;
import java.util.Collections;
import static java.util.Collections.list;

/**
 *
 * @author ipd11
 */
class Person {
	String name;
	Gender gender;
	AgeRange ageRange;
        enum Gender { Male, Female, NA }
	enum AgeRange { Below18, From18to35, From35to65, Over65 }
        @Override
	public String toString() {
            return name + "  is a " + gender+ " " + ageRange;
        }
      
}
 class PersonFileAccess {

}
public class EnumPerson {

    /**
     * @param args the command line arguments
     */
     

    public static void main(String[] args) throws FileNotFoundException {
        // TODO code application logic here
      ArrayList<Person> list = new ArrayList<>();
        Scanner in=new Scanner(new File("input.txt"));
        while (in.hasNextLine()){
        String line=in.nextLine();
        String data[]=line.split(";");
        Person p=new Person();
        p.name=data[0];
        p.gender=Person.Gender.valueOf(data[1]);
        p.ageRange=Person.AgeRange.valueOf(data[2]);
       list.add(p); 
    }
        for(Person p:list){
        System.out.println(p);
        }
    }
    
}
